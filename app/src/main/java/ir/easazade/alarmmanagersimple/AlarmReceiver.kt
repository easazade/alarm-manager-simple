package ir.easazade.alarmmanagersimple

import android.app.Notification
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat

class AlarmReceiver : BroadcastReceiver() {

  override fun onReceive(context: Context, intent: Intent) {
//    val manager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
//    val alarmio = context.applicationContext as App
//    val alarm = alarmio.getAlarms().get(intent.getIntExtra(EXTRA_ALARM_ID, 0))
//    if (alarm.isRepeat())
//      alarm.set(context, manager)
//    else
//      alarm.setEnabled(alarmio, manager, false)
//    alarmio.onAlarmsChanged()
//
//    val ringer = Intent(context, AlarmActivity::class.java)
//    ringer.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//    ringer.putExtra(AlarmActivity.EXTRA_ALARM, alarm)
//    context.startActivity(ringer)
    val id = intent.getIntExtra(EXTRA_ALARM_ID, -1)
    if (id != -1) {
      val dao = Db.getInstance(context).alarmsDao()
      val alarm = dao.getAlarmById(id)
      if (alarm != null) {
        dao.deleteAlarm(id)
        if (alarm.showsNotification)
          NotificationManagerCompat.from(context).notify(21, createAlarmNotification(context))
        if (alarm.opensActivity) {
          val ringerIntent = Intent(context, RingActivity::class.java)
          ringerIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
          context.startActivity(ringerIntent)
        }
      }
    }
  }

  fun createAlarmNotification(context: Context): Notification =
    NotificationCompat.Builder(context, App.SECONDARY_CHANNEL_ID)
      .setSmallIcon(R.mipmap.ic_launcher_round)
      .setContentTitle("وقت دعاست")
      .setContentText("با خلوص نیت همه با هم برای فرج آقا امام زمان دعا می کنیم")
      .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
      .setPriority(NotificationCompat.PRIORITY_MAX)
      .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
      .setAutoCancel(false) //automatically removes notification when tapped
      .build()

  companion object {
    val EXTRA_ALARM_ID = "james.alarmio.EXTRA_ALARM_ID"
  }
}