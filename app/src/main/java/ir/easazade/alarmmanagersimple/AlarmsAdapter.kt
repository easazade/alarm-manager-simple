package ir.easazade.alarmmanagersimple

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ir.easazade.alarmmanagersimple.AlarmsAdapter.AlarmVH

class AlarmsAdapter(
  var alarms: MutableList<AlarmData>
) : RecyclerView.Adapter<AlarmVH>() {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = AlarmVH(
    LayoutInflater.from(parent.context).inflate(R.layout.alarm_list_item, parent, false)
  )

  override fun getItemCount(): Int = alarms.size

  override fun onBindViewHolder(holder: AlarmVH, position: Int) {
    val alarm = alarms[position]
    holder.name.text = alarm.name
    holder.iconActivity.visibility = if (alarm.opensActivity) View.VISIBLE else View.INVISIBLE
    holder.iconNotiff.visibility = if (alarm.showsNotification) View.VISIBLE else View.INVISIBLE
  }

  class AlarmVH(root: View) : RecyclerView.ViewHolder(root) {
    val name: TextView = root.findViewById(R.id.item_alarm_name)
    val iconActivity: ImageView = root.findViewById(R.id.item_alarm_icActivity)
    val iconNotiff: ImageView = root.findViewById(R.id.item_alarm_icNotification)
  }
}