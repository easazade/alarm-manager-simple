package ir.easazade.alarmmanagersimple

import android.app.AlarmManager
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_new_alarm.hour
import kotlinx.android.synthetic.main.activity_new_alarm.launchesActivity
import kotlinx.android.synthetic.main.activity_new_alarm.minute
import kotlinx.android.synthetic.main.activity_new_alarm.setAlarm
import kotlinx.android.synthetic.main.activity_new_alarm.showsNotification
import java.sql.Timestamp
import java.util.Calendar
import kotlin.random.Random

class NewAlarmActivity : AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_new_alarm)

    setAlarm.setOnClickListener {
      val alarm = AlarmData(
        Random(System.currentTimeMillis()).nextInt(),
        "alarm name",
        getTimestampOf(hour.text.toString().toInt(), minute.text.toString().toInt()).time,
        showsNotification.isChecked,
        launchesActivity.isChecked
      )
      alarm.set(this, getSystemService(Context.ALARM_SERVICE) as AlarmManager)
    }
  }

  fun getTimestampOf(hour: Int, minute: Int): Timestamp {
    val cal = Calendar.getInstance()
    cal.time = Timestamp(System.currentTimeMillis())
    cal.set(Calendar.HOUR_OF_DAY, hour)
    cal.set(Calendar.MINUTE, minute)
    return Timestamp(cal.timeInMillis)
  }
}
