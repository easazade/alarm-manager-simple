package ir.easazade.alarmmanagersimple

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_alarms_list.addNewAlarmBtn
import kotlinx.android.synthetic.main.activity_alarms_list.alarmsList

class AlarmsListActivity : AppCompatActivity() {

  private lateinit var mAdapter: AlarmsAdapter

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_alarms_list)
    mAdapter = AlarmsAdapter(Db.getInstance(this).alarmsDao().allAlarms().toMutableList())
    alarmsList.layoutManager = LinearLayoutManager(this)
    alarmsList.adapter = mAdapter

    addNewAlarmBtn.setOnClickListener {
      val intent = Intent(this, NewAlarmActivity::class.java)
      startActivity(intent)
    }
  }

  override fun onResume() {
    super.onResume()
    val alarms = Db.getInstance(this).alarmsDao().allAlarms().toMutableList()
    mAdapter.alarms = alarms
    mAdapter.notifyDataSetChanged()
  }
}
