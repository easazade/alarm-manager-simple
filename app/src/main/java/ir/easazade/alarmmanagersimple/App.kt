package ir.easazade.alarmmanagersimple

import android.app.Application
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.os.Build

class App : Application(){

  companion object{
    const val SECONDARY_CHANNEL_ID = "ss app_channel_id"
  }

  override fun onCreate() {
    super.onCreate()
    createNotificationChannelWithSoundAndVibration()

  }

  private fun createNotificationChannelWithSoundAndVibration() {
    // Create the NotificationChannel, but only on API 26+ because
    // the NotificationChannel class is new and not in the support library
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      val alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
      val name = getString(R.string.app_notification_channel_name)
      val descriptionText = getString(R.string.app_notification_channel_description)
      val importance = NotificationManager.IMPORTANCE_MAX
      val channel = NotificationChannel(SECONDARY_CHANNEL_ID, name, importance).apply {
        description = descriptionText
        enableVibration(true)
        val audioAttributes = AudioAttributes.Builder()
          .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
          .setUsage(AudioAttributes.USAGE_NOTIFICATION)
          .build()
        setSound(alarmSound, audioAttributes)
        vibrationPattern = longArrayOf(0, 300, 200, 300)
        lockscreenVisibility = Notification.VISIBILITY_PUBLIC

      }
      // Register the channel with the system
      val notificationManager: NotificationManager =
        getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
      notificationManager.createNotificationChannel(channel)
    }
  }

}