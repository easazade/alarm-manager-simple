package ir.easazade.alarmmanagersimple

import android.content.Context
import androidx.room.Dao
import androidx.room.Database
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [AlarmData::class], version = 4)
abstract class AppDatabase() : RoomDatabase() {

  abstract fun alarmsDao(): AlarmsDao
}

@Dao
interface AlarmsDao {

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  fun saveAlarm(alarmData: AlarmData)

  @Query("DELETE FROM alarmdata WHERE id=:alarmId")
  fun deleteAlarm(alarmId: Int)

  @Query("SELECT * FROM alarmdata")
  fun allAlarms(): List<AlarmData>

  @Query("SELECT * FROM alarmdata WHERE id=:id")
  fun getAlarmById(id: Int): AlarmData?
}

class Db() {

  companion object {
    private var INSTNACE: AppDatabase? = null
    fun getInstance(context: Context): AppDatabase {
      val app = context.applicationContext as App
      if (INSTNACE == null)
        INSTNACE = Room.databaseBuilder(app, AppDatabase::class.java, "app_database_alarm")
          .fallbackToDestructiveMigration()
          .allowMainThreadQueries()
          .build()
      return INSTNACE!!
    }
  }
}