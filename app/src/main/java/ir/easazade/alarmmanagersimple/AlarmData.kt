package ir.easazade.alarmmanagersimple

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class AlarmData(
  @PrimaryKey
  val id: Int,
  val name: String,
  val time: Long,
  val showsNotification: Boolean = true,
  val opensActivity: Boolean = false
) {

  /**
   * The intent to fire when the alarm should ring.
   *
   * @param context       An active context instance.
   * @return              A PendingIntent that will open the alert screen.
   */
  private fun getIntent(context: Context): PendingIntent {
    val intent = Intent(context, AlarmReceiver::class.java)
    intent.putExtra(AlarmReceiver.EXTRA_ALARM_ID, id)
    return PendingIntent.getBroadcast(context, id, intent, PendingIntent.FLAG_UPDATE_CURRENT)
  }

  /**
   * Schedule a timestamp for the alarm to ring at.
   *
   * @param context       An active context instance.
   * @param manager       The AlarmManager to schedule the alarm on.
   * @param timeMillis    A UNIX timestamp specifying the next timestamp for the alarm to ring.
   */
  fun set(context: Context, manager: AlarmManager) {
    //saving alarm
    Db.getInstance(context).alarmsDao().saveAlarm(this)
    //setting alarm
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      manager.setAlarmClock(
        AlarmManager.AlarmClockInfo(
          time,
          PendingIntent.getActivity(context, 0, Intent(context, NewAlarmActivity::class.java), 0)
        ),
        getIntent(context)
      )
    } else {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        manager.setExact(AlarmManager.RTC_WAKEUP, time, getIntent(context))
      else
        manager.set(AlarmManager.RTC_WAKEUP, time, getIntent(context))

      val intent = Intent("android.intent.action.ALARM_CHANGED")
      intent.putExtra("alarmSet", true)
      context.sendBroadcast(intent)
    }

//    manager.set(
//      AlarmManager.RTC_WAKEUP,
//      timestamp.timestamp - PreferenceData.SLEEP_REMINDER_TIME.getValue(context) as Long,
//      PendingIntent.getService(context, 0, Intent(context, SleepReminderService::class.java), 0)
//    )
//
//    SleepReminderService.refreshSleepTime(context)
  }

  /**
   * Cancel the next time for the alarm to ring.
   *
   * @param context       An active context instance.
   * @param manager       The AlarmManager that the alarm was scheduled on.
   */
  fun cancel(context: Context, manager: AlarmManager) {
    manager.cancel(getIntent(context))

    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
      val intent = Intent("android.intent.action.ALARM_CHANGED")
      intent.putExtra("alarmSet", false)
      context.sendBroadcast(intent)
    }
  }
}